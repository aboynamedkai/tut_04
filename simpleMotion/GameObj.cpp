#include "GameObj.h"
#include "SFML/Graphics.hpp"
#include "Application.h"

void GameObj::Update()
{
  if (mVel.x != 0 || mVel.y != 0)
  {
    sf::Vector2f pos{ mSprite.getPosition() };
    pos += mVel * Application::GetElapsedSecs();
    mSprite.setPosition(pos);
  }
}