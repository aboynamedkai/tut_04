#pragma once
#include "GameObj.h"
//cannon balls bounce off walls
class Wall : public GameObj
{
public:
  static const int MAX_WALLS = 4;				//a wall on each side of the screen
  enum WallType { LEFT, RIGHT, TOP, BOTTOM };	//identify where each wall is

  inline void Wall::Update()
  {
    //add ball collision code here
  }

  Wall()
    :GameObj(), mType(LEFT)
  {}
  void Init(WallType wtype) {
    mType = wtype;
  }

private:
  WallType mType; //where does this instance fit on screen
};

