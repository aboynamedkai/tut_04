#pragma once
#include "Wall.h"
#include "Gun.h"
#include "SFML/Graphics.hpp"
using namespace std;
//in this mode the player can spin the cannon, like a game's playable part
//similar structure to the NameMode, imagine more of these objects for each
//significant part of the game, all structured the same way

class Game;

class PlayMode
{
public:
  PlayMode() : mpGame(nullptr) {}
  void Init(Game*);
  void Update();
  void Render();
private:
  Game *mpGame; //for communication

  sf::Texture mCannonTex;			//cannon and ball
  sf::Texture mWallTex;			//walls
  Wall mWalls[Wall::MAX_WALLS];	//four walls
  Gun mGun;		//cannon
};
